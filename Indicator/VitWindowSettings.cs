#region License and Terms

/*
The MIT License (MIT)

Copyright (c) 2013-2016 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#endregion License and Terms

#region Using declarations

using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.Serialization;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;

#endregion Using declarations

namespace NinjaTrader.Indicator {

	[Description("Vitalij's window settings indicator for NinjaTrader 7" + "\r\n" + "http://ninjatrader.bitbucket.org/")]
	public class VitWindowSettings : IndicatorBase {
		private bool apply = false;
		private Form frm;
		private Form btn;

		protected override void Initialize() {
			Overlay = true;
			CalculateOnBarClose = true;

			Name = "Vitalij's Window Settings Indicator";

			// search proper form
			if (("" + Form.ActiveForm).Contains("NinjaTrader.Gui.Chart.ChartIndicators")) {
				InitializeProperties((ChartControl)Form.ActiveForm.GetType().GetField("chartControl", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(Form.ActiveForm));
			}
		}

		protected override void OnStartUp() {
			ChartControl.ChartPanel.DoubleClick += OnDoubleClick;

			frm = ChartControl.ParentForm;
			btn = frm.OwnedForms.First();

			ApplySettings();
		}

		protected override void OnTermination() {
			ChartControl.ChartPanel.DoubleClick -= OnDoubleClick;

			//if (!frm.IsDisposed && !frm.Disposing) {
			//    FailSafeDefaults();
			//}
		}

		private void InitializeProperties(ChartControl chartControl) {
			Form frm = chartControl.ParentForm;
			Form btn = frm.OwnedForms.First();

			// Location
			Left = frm.Location.X;
			Top = frm.Location.Y;
			EnforceLeft = false;
			EnforceTop = false;

			// Size
			Width = frm.Size.Width;
			Height = frm.Size.Height;
			EnforceWidth = false;
			EnforceHeight = false;

			// Border Style
			WindowBorderStyle = frm.FormBorderStyle;
			chartControl.ChartPanel.BorderStyle = BorderStyle.None;

			// Miscellaneous (Watch out!)
			Modal = frm.TopMost;
			Transparent = frm.AllowTransparency;
			ShowInTaskbar = frm.ShowInTaskbar;
			WindowState = frm.WindowState;

			// Miscellaneous
			HideButton = !btn.Visible;
			ForceMaxZOrder = false;
		}

		private void OnDoubleClick(object sender, EventArgs args) {
			if (apply) {
				ApplySettings();
			} else {
				FailSafeDefaults();
			}

			apply = !apply;
		}

		private void ApplySettings() {
			// Location & Size
			if (EnforceLeft) { frm.Left = Left; } else { Left = frm.Left; }
			if (EnforceTop) { frm.Top = Top; } else { Top = frm.Top; }
			if (EnforceWidth) { frm.Width = Width; } else { Width = frm.Width; }
			if (EnforceHeight) { frm.Height = Height; } else { Height = frm.Height; }

			using (new FormState(frm)) {
				// Border Style
				frm.FormBorderStyle = WindowBorderStyle;
				ChartControl.ChartPanel.BorderStyle = ChartBorderStyle;

				// Miscellaneous (Watch out!)
				frm.TopMost = Modal;
				frm.TransparencyKey = ChartControl.ChartPanel.BackColor;
				frm.AllowTransparency = Transparent;
				frm.ShowInTaskbar = ShowInTaskbar;
				//frm.WindowState = WindowState;
			}

			// Miscellaneous
			btn.Visible = !HideButton;
			btn.TopMost = Modal;
			if (ForceMaxZOrder) {
				Bars.BarsData.ZOrder = int.MaxValue;
			}

			frm.TopMost = Modal;
		}

		private void FailSafeDefaults() {
			using (new FormState(frm)) {
				// Border Style
				frm.FormBorderStyle = FormBorderStyle.Sizable;
				ChartControl.ChartPanel.BorderStyle = BorderStyle.None;

				// Miscellaneous (Watch out!)
				frm.TopMost = false;
				frm.AllowTransparency = false;
				frm.ShowInTaskbar = true;
				//frm.WindowState = FormWindowState.Normal;
			}

			// Miscellaneous
			btn.Visible = true;
			btn.TopMost = false;

			frm.TopMost = false;
		}

		#region Helpers

		// dummy
		protected override void OnBarUpdate() {
		}

		// class to restore form location and size
		private class FormState : IDisposable {
			private Form frm;
			private int left, top, width, height;

			public FormState(Form frm) {
				this.frm = frm;
				this.left = frm.Left;
				this.top = frm.Top;
				this.width = frm.Width;
				this.height = frm.Height;
			}

			public void Dispose() {
				frm.Left = left;
				frm.Top = top;
				frm.Width = width;
				frm.Height = height;
			}
		}

		// helper to serialize Enums
		private T ParseEnum<T>(string value) {
			return (T)Enum.Parse(typeof(T), value, true);
		}

		#endregion Helpers

		#region Properties

		[Category("01. Location")]
		[Gui.Design.DisplayName("01. Left")]
		public int Left { get; set; }

		[Category("01. Location")]
		[Gui.Design.DisplayName("02. Top")]
		public int Top { get; set; }

		[Category("01. Location")]
		[Gui.Design.DisplayName("03. Enforce Left")]
		[Description("Enforce given window location coordinates on indicator reload/refresh.")]
		public bool EnforceLeft { get; set; }

		[Category("01. Location")]
		[Gui.Design.DisplayName("04. Enforce Top")]
		[Description("Enforce given window location coordinates on indicator reload/refresh.")]
		public bool EnforceTop { get; set; }

		[Category("02. Size")]
		[Gui.Design.DisplayName("01. Width")]
		public int Width { get; set; }

		[Category("02. Size")]
		[Gui.Design.DisplayName("02. Height")]
		public int Height { get; set; }

		[Category("02. Size")]
		[Gui.Design.DisplayName("03. Enforce Width")]
		[Description("Enforce given window size dimensions on indicator reload/refresh.")]
		public bool EnforceWidth { get; set; }

		[Category("02. Size")]
		[Gui.Design.DisplayName("04. Enforce Height")]
		[Description("Enforce given window size dimensions on indicator reload/refresh.")]
		public bool EnforceHeight { get; set; }

		[XmlIgnore()]
		[Category("03. Border Style")]
		[Gui.Design.DisplayName("01. Window")]
		public FormBorderStyle WindowBorderStyle { get; set; }

		[Browsable(false)]
		public string WindowBorderStyleSerialize {
			get { return WindowBorderStyle.ToString(); }
			set { WindowBorderStyle = ParseEnum<FormBorderStyle>(value); }
		}

		[XmlIgnore()]
		[Category("03. Border Style")]
		[Gui.Design.DisplayName("02. Chart")]
		public BorderStyle ChartBorderStyle { get; set; }

		[Browsable(false)]
		public string ChartBorderStyleSerialize {
			get { return ChartBorderStyle.ToString(); }
			set { ChartBorderStyle = ParseEnum<BorderStyle>(value); }
		}

		[Category("04. Miscellaneous (Watch out!)")]
		[Gui.Design.DisplayName("01. Modal")]
		public bool Modal { get; set; }

		[Category("04. Miscellaneous (Watch out!)")]
		[Gui.Design.DisplayName("02. Transparent")]
		public bool Transparent { get; set; }

		[Category("04. Miscellaneous (Watch out!)")]
		[Gui.Design.DisplayName("03. Show in taskbar")]
		public bool ShowInTaskbar { get; set; }

		[XmlIgnore()]
		[Category("04. Miscellaneous (Watch out!)")]
		[Gui.Design.DisplayName("04. Window style")]
		[Description("Start your workspace with minimized charts.")]
		public FormWindowState WindowState { get; set; }

		[Browsable(false)]
		public string WindowStateSerialize {
			get { return WindowState.ToString(); }
			set { WindowState = ParseEnum<FormWindowState>(value); }
		}

		[Category("05. Miscellaneous")]
		[Gui.Design.DisplayName("01. Hide GroupButton")]
		[Description("Hide chart-group button.")]
		public bool HideButton { get; set; }

		[Category("05. Miscellaneous")]
		[Gui.Design.DisplayName("02. Force max ZOrder")]
		[Description("Force best visibility for dataseries.")]
		public bool ForceMaxZOrder { get; set; }

		#endregion Properties
	}
}
